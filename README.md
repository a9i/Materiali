# Italian Linux Society - materiali

Raccolta di materiali per l'attività associativa Italian Linux Society.

## Licenza

Salvo ove diversamente indicato, tutti i contributi in questo repository (sia testo che immagini o altri materiali multimediali) sono pubblicati in pubblico dominio (Creative Commons Zero).

https://creativecommons.org/publicdomain/zero/1.0/
