#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import datetime as dt
import subprocess
import sys
import os

try:
    type_csv = sys.argv[1]
except:
    print("Devi scegliere il tipo di sondaggio: soci|nonsoci|completo")
    sys.exit()

print("Elaborazione per " + type_csv)

path = './' + type_csv
if not os.path.isdir(path):
    print("Creo la cartella")
    os.mkdir('./' + type_csv)

ils = pd.read_csv('2021-ils-survey-' + type_csv + '.csv')

if not os.path.isfile(path + '/' + type_csv + '.txt'):
    print("Genero versione testuale")
    text = ''
    for col in ils.columns:
        text += "*************************\n"
        text += col
        text += str(ils[col].value_counts(dropna=False))

    textsave = open(path + "/" + type_csv + ".txt","w")
    textsave.writelines(text)
    textsave.close()

i = 0
print("Creazione SVG")
if len(ils.columns) == 2:
    plt.xkcd()
    plot = plt.barh(ils["Quanti anni hai?"],ils["Totale"], color=np.random.rand(len(ils["Totale"]),3))
    plt.title('Età totale degli associati',fontweight="bold")
    ax = plt.gca()
    total = 210 # Totali dei soci per fare la percentuale
    for p in ax.patches:
        percentage = '{:.1f}%'.format(100 * p.get_width()/total)
        x = p.get_x() + p.get_width() + 0.02
        y = p.get_y() + p.get_height()/2
        ax.annotate(percentage, (x, y))
    plt.savefig(path + '/' + str(i) + '.svgz', bbox_inches="tight")
else:
    for col in ils.columns:
        i += 1
        if not os.path.isfile(path + '/' + str(i) + '.svgz'):
            print("Generazione grafico " + str(i))
            plt.clf()
            plt.xkcd()
            plt.tight_layout()
            if "provincia" in col:
                plt.title(col)
                labels = []
                for v, group in ils.groupby([col]):
                    labels.append(v)

                counts = ils[col].value_counts(ascending=True).reindex(labels)
                l = [f'{yn} ({c})' for yn,c in counts.iteritems()]

                pie = plt.pie(x=ils.groupby([col]).size(), labels=labels)
                plt.legend(pie[0], l, loc="lower right", bbox_to_anchor=(0.9, -1), bbox_transform=plt.gcf().transFigure, ncol=3)
            else:
                plt.plot(legend=None)
                plt.title(col.replace('[','\n['),fontweight="bold")
                order = ils[col].value_counts().sort_index().index
                ax = sns.countplot(y=col, data=ils, dodge=True, order=order)
                label = 'Tutti i partecipanti'
                if type_csv == 'soci':
                    label = 'Solo i soci'
                elif type_csv == 'nonsoci':
                    label = 'Solo i non soci'
                elif type_csv == '35sotto':
                    label = 'Sotto i 35 anni di età'
                elif type_csv == '35sopra':
                    label = 'Sopra i 35 anni di età'
                ax.set(xlabel=label)
                ax.set(ylabel=None)
                ax.xaxis.label.set_color("green")
                total = len(ils[col])
                for p in ax.patches:
                        percentage = '{:.1f}%'.format(100 * p.get_width()/total)
                        x = p.get_x() + p.get_width() + 0.02
                        y = p.get_y() + p.get_height()/2
                        ax.annotate(percentage, (x, y))

            plt.savefig(path + '/' + str(i) + '.svgz', bbox_inches="tight")
print("SVG generati")

if not os.path.isfile(path + '/' + type_csv + '.pdf'):
    print("PDF in corso")
    subprocess.run('convert -density 200 $(/bin/ls -1 -v ' + path + '/*.svgz) ' + path + '/' + type_csv + '.pdf',shell=True)
    print("PDF generato")
