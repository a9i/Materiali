# Sondaggio 2021

Il sondaggio è stato effettuato dal 24/03 fino al 02/05 del 2021, aperto ai soci e non di Italian Linux Society con 173 partecipanti su tutto il paese.

Questa cartella contiene i dati sotto forma di grafici e testo aggregati.

Il file `analisi.md` con la versione `analisi.pdf` include un riassunto elaborando i dati raccolti e presenti su questo archivio.  
Sono presenti dei file CSV già divisi, di cui uno è la versione aggregata e gli altri due sono differenziati tra soci e non.

## Come funziona

Ogni cartella include un grafico per ogni risposta in svgz (svg compresso), un file pdf che aggrega tutto e un file txt che contiene la versione testuale per domanda.

### Script

```
genera-pdf.py soci|nonsoci|40sopra|40sotto|completo
genera.sh
```

## Crediti

* Valerio Bozzolan per la prima versione dei grafici su LibreOffice e analisi
* Stefania Delprete per la consulenza lato dati e python
* Daniele Scasciafratte per gli script, riassunto e analisi

## Licenza 

GPLv3 per il file `genera-pdf.py` mentre il resto è tutto [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.it).

Il sondaggio è stato fatto con [LimeSurvey](https://limesurvey.org/).