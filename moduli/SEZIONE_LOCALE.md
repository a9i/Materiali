# Apertura sezione locale ILS

Questa bozza può essere adattata per richiedere l'apertura di una nuova sezione locale in Italian Linux Society.

Su https://www.ils.org/sezionilocali/ puoi verificare se esistono già sezioni locali nella tua zona; se non esistono e vuoi promuoverne tu la costituzione le linee guida ti spiegheranno la procedura di attivazione e i ti daranno qualche indicazione sui criteri di gestione.


## Vantaggi

Se non si è sicuri se sia il caso aprire una sezione locale, ecco alcuni vantaggi:

* meno burocrazia
* ...

## Modulo

Questo tipo di richieste vengono mandate più o meno analogalmente al template in basso. Sostituisci `<CITTÀ>` e modifica pure qui e lì.

```
To: direttivo@linux.it
Subject: Sezione locale ILS <CITTÀ>

Un saluto ai soci,

I soci ILS:

* Gina Pina
* Mario Rossi
* [...] (occorrono almeno tre soci)

Richiedono la costituzione di una sezione locale ILS a <CITTÀ>, che
agisca da collettore per le realtà (associative e non) in cui siamo
attualmente coinvolti:

* Linux Day <CITTÀ>
* ...

Di fatto negli anni passati, insieme a moltissimi altri, abbiamo già
agito come sezione locale, appoggiandoci a ILS in particolare per le
sponsorizzazioni e le richieste di spazi per il Linux Day.

Grazie a tutti!
```
